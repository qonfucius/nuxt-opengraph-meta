# Custom Nuxt opengraph meta
With this module you can automatically integrate the open graph and Twitter cards tags.
[Documentation](https://qonfucius.gitlab.io/nuxt-opengraph-meta/)

## Installation
Add this library in dependencies, then add it to nuxtJS modules
```sh
npm install @qonfucius/nuxt-opengraph-meta
```
Edit your nuxt.config.js
```js
// nuxt.config.js
export default {
  modules: [
    '@qonfucius/nuxt-opengraph-meta',
  ],
};
```
## Demo
Inside `examples` folder
```bash
# serve with hot reload at localhost:3000
$ npm run dev
```
## Basic example
Implement a `ogMeta` object with `og:`key in your vue script.
```js
//example.vue
<script>
  export default {
    ogMeta: {
        'og:description' : 'A beautiful description of the page',
    },
  }
</script>
```
Or in `nuxt.config.js` with nuxtOpengraphMeta in option.

```js
//nuxt.config.js
modules: [
  // Module with option
  ['@qonfucius/nuxt-opengraph-meta',{
    nuxtOpengraphMeta: {
    'og:type' : 'my_namespace:my_type',
    },
  }]
],
```
Automatically generated part `<head>` are:
```html
<meta data-n-head="ssr" data-hid="property:og:description" property="og:description" content="A beautiful description of your page">
<meta data-n-head="ssr" data-hid="property:og:type" property="og:type" content="my_namespace:my_type">
```
## Usage
1. [Properties](https://qonfucius.gitlab.io/nuxt-opengraph-meta/properties.html)
2. [Protocol](https://qonfucius.gitlab.io/nuxt-opengraph-meta/protocol.html)
3. [Use Nuxt context](https://qonfucius.gitlab.io/nuxt-opengraph-meta/nuxt-context.html)


