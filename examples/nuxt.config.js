export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'example-ogmeta',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
  // Module with option
    ['@qonfucius/nuxt-opengraph-meta',{
        forceProtocol: 'https',
        nuxtOpengraphMeta: {
          'og:type' : 'my_namespace:my_type',
        },
    }]
  ],
  //Module ogMeta.nuxtOpengraph
  /*ogMeta: {
     nuxtOpengraphMeta:{
      'twitter:card' : 'app',
    },
  },*/
  //Use Nuxt context
  //ogMeta: (context) => ({
   // 'og:url': `${context.route}`,
  //}),
}
