## Protocol
For `og:url`,you can specify or force the protocol. 

### defaultProtocol
`defaultProtocol` allows you to specify the default protocol if Node.js cannot detect the protocol used.

```js
//nuxt.config.js
  modules: [
  // Module with option
    ['@qonfucius/nuxt-opengraph-meta',{
        defaultProtocol: 'http',
        nuxtOpengraphMeta: {
          'og:type' : 'my_namespace:my_type',
        },
    }]
  ],
```
Automatically generated part is:
```html
<meta data-n-head="ssr" data-hid="property:og:url" property="og:url" content="http://localhost:3000/">
```

### forceProtocol
`forceProtocol` allows you to force he protocol used.

  ```js
//nuxt.config.js
  modules: [
  // Module with option
    ['@qonfucius/nuxt-opengraph-meta',{
        forceProtocol: 'https',
        nuxtOpengraphMeta: {
          'og:type' : 'my_namespace:my_type',
        },
    }]
  ],
  ```
  Automatically generated part is:
  ```html
  <meta data-n-head="ssr" data-hid="property:og:url" property="og:url" content="https://localhost:3000/">
  ```