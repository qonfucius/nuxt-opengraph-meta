import { resolve, join } from 'path';

const MODULE_DIR = 'qnuxtopengraphmeta';

export default async function nuxtCustomHeaders (options = {}) {
    const OPTIONS_KEY = options.OPTIONS_KEY || 'ogMeta';

    this.addPlugin({
        src: resolve(__dirname, 'middleware.js'),
        fileName: join(MODULE_DIR, 'middleware.js'),
        options: {
            OPTIONS_KEY,
            defaultProtocol: (this.options[OPTIONS_KEY] || {}).defaultProtocol || options.defaultProtocol,
            forceProtocol: (this.options[OPTIONS_KEY] || {}).forceProtocol || options.forceProtocol,
            nuxtOpengraphMeta: [
                (this.options[OPTIONS_KEY] || {}).nuxtOpengraphMeta,
                options.nuxtOpengraphMeta,
            ],
        }
    });
    this.options.router.middleware.push(OPTIONS_KEY);
};

