# Changelog

### [v1.0.1](https://gitlab.com/qonfucius/nuxt-opengraph-meta/compare/v1.0.0...v1.0.1) (2020-11-23)

#### Fixes

* Lookup into `res` when undefined 4ee4b84


## v1.0.0 (2020-11-20)

### Features

* First module release c67ec7c


