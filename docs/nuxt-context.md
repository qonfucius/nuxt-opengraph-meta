## Use Nuxt Context

Via `ogMeta`, you can use the Nuxt context to generate headers based on the router or any other context related data.

```js
//nuxt.config.js
ogMeta: (context) => ({
    'og:url': `${context.route}`,
  }),
```
Or in the component.

```js
//example.vue
ogMeta: (context) => ({
        'og:url': `${context.params.slug='mySlug'}`,
        }),
```

:exclamation:
Tags defined in `nuxt.config.js` are "overwritten" by the same tags defined in vue file.