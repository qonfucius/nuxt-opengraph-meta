## Properties

Default properties with nuxt-opengraph-meta
```js
'og:url', url ? url.toString() : null;
'og:type', 'website';
'og:title', title || null;
'og:description', description || null;
'og:image';
'twitter:card', 'summary';

'twitter:title', title || null;
'twitter:description', description || null;
'twitter:image';
'twitter:creator';
'twitter:site';
```

You can also add other Open Graph Meta with `head()` method.

```js
//example.vue
export default {  
    name: 'MyPage',
    head() {
        return {
            meta: [
                {
                hid: 'property:og:video',
                property: 'og:video',
                content:'A cat video',
                }
            ]
        }
    },
}
```
