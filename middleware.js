import middleware from '../middleware';

middleware['<%=options.OPTIONS_KEY%>'] = async (context) => {
    let url;
    if (context.req) {
        url = new URL(`${<%=(options.forceProtocol ? JSON.stringify(options.forceProtocol) : 'context.req.protocol')%> || '<%=(options.defaultProtocol || "http")%>'}://${context.req.headers.host}${context.req.originalUrl}`);
    }
    let mergedData = {};
    <% for (const source of options.nuxtOpengraphMeta) { %>
        <% if (typeof source === 'object' || typeof source === 'function') { %>
            <% if (typeof source === 'function') { %>
            Object.assign(mergedData, (<%=source.toString()%>)(context));
            <% } else { %>
            Object.assign(mergedData, <%=JSON.stringify(source) %>);
            <% } %>
        <% } %>
    <% } %>
    context.route.matched
        .map(({ components }) => Object.values(components))
        .reduce((acc, row) => acc.concat(row), [])
        .forEach(component => {
        const object = (component.options && component.options["<%=options.OPTIONS_KEY%>"]);
        const customHead = (component.options.head || (() => ({})));
        component.options.head = function socialHead(...params) {
            const nuxtOpengraphMeta = mergedData;
            if (typeof object === 'function') {
                Object.assign(nuxtOpengraphMeta, object(context));
            } else if (typeof object === 'object') {
                Object.assign(nuxtOpengraphMeta, object);
            }
            const head = customHead.call(this, ...params);
            const meta = head.meta || [];
            const description = (meta.find(r => r.hid === 'description') || {}).content ;
            const title = head.title;
            // const explicitlyCalled = [];
            const overrideOrPush = (kind, key, defaultContent = null) => {
                const content = nuxtOpengraphMeta[key] || defaultContent;
                const hid = `${kind}:${key}`;
                if (nuxtOpengraphMeta[hid] !== false) {
                    const oldValue = meta.find(r => r.hid === hid);
                    if (content) {
                        if (oldValue) {
                            Object.assign(oldValue, {[kind]: key, content });
                        } else {
                            meta.push({hid, [kind]: key, content});
                        }
                    }
                }
            }
            const overrideOrPushName = overrideOrPush.bind(this, 'name');
            const overrideOrPushProperty = overrideOrPush.bind(this, 'property');

            // Known opengraph tags
            overrideOrPushProperty('og:url', url ? url.toString() : null);
            overrideOrPushProperty('og:type', 'website');
            overrideOrPushProperty('og:title', title || null);
            overrideOrPushProperty('og:description', description || null);
            overrideOrPushProperty('og:image');

            overrideOrPushName('twitter:card', 'summary');
            overrideOrPushName('twitter:title', title || null);
            overrideOrPushName('twitter:description', description || null);
            overrideOrPushName('twitter:image');
            overrideOrPushName('twitter:creator');
            overrideOrPushName('twitter:site');

            return {
                ...head,
                meta,
            };
        }
    });
}
